package com.capybaras.tp2.core;

import java.util.ArrayList;

public class Sudoku {
	
	private ArrayList<Character> _grille;

	public Sudoku() {
		
	}
	
	public void setGrille(ArrayList<Character> grille) {
		_grille = grille;
	}
	
	public ArrayList<Character> getGrille() {
		return _grille;
	}
}
