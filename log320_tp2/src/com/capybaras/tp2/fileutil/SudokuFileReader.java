package com.capybaras.tp2.fileutil;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;

public class SudokuFileReader {

	private static Reader reader;
	
	public static ArrayList<Character> readFile(String fileName) throws IOException {
		ArrayList<Character> arrayList = new ArrayList<Character>(81);
		
		try {
			FileInputStream in = new FileInputStream(fileName);
			reader = new BufferedReader( new InputStreamReader(in, "UTF-8") );
			char[] buffer = new char[9];
			int nbChar = 0, cptRow = 0;
			while((nbChar = reader.read(buffer)) != -1 && cptRow < 9) {
				if(nbChar < 9) {
					System.out.println("Erreur lors de la lecture du fichier sudoku: Une des lignes du tableau d'entree contient moins de 9 caracteres.");
					arrayList.clear();
					return arrayList;
				}
				for(char character : buffer) {
					arrayList.add(character);
				}
				++cptRow;
			}
			return arrayList;
		}
		catch(IOException e) {
			System.out.println("Erreur lors de la lecture du fichier sudoku: " + e.toString());
			arrayList.clear();
			return arrayList;
		}
		finally {
			reader.close();
		}
	}
	
}
